**Acceptance criteria**  
- [ ] Coverage is higher or equal to coverage on master ![coverage](https://gitlab.com/stp-team/systemtestportal/badges/master/coverage.svg?job=test-project)  
- [ ] CI runs without problems  
- [ ] Linter does not contain any additional issues  
- [ ] Code does not contain variables with abbreviated names
- [ ] Code doesn't contain TODOs, unused functions or console outputs  
- [ ] Code has been inspected and deemed to have decent quality  
- [ ] Code does contain license headers  
- [ ] If necessary, the changes are documented in the changelog  
- [ ] If this is a bug fix, a test covering the bug is present

[effectiveGo]: https://golang.org/doc/effective_go.html  