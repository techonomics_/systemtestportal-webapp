/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

func TestNewSequenceExecutionProtocol(t *testing.T) {
	const SeqVersErrorMessage = "TestSequenceVersionID wasn't transferred correctly to new Protocol. " +
		"Value in new Protocol is %+v but expected %+v"
	const SUTVariantErrorMessage = "SUTVariant wasn't transferred correctly to new Protocol. " +
		"Value in new Protocol is %q but expected %q"
	const SUTVersErrorMessage = "SUTVersion wasn't transferred correctly to new Protocol. " +
		"Value in new Protocol is %q but expected %q"
	const TimeErrorMessage = "Needed Time wasn't transferred correctly to new Protocol. " +
		"Value in new Protocol is %v+ but expected %v+"

	tsvNr := 0

	ProjectID := id.NewProjectID(id.NewActorID("owner"), "d8ed5f42")

	TestCase1 := NewTestCase("Case 1", "Do things", "booted", []project.Label{},
		map[string]*project.Variant{}, duration.NewDuration(0, 5, 0), ProjectID)
	TestCase2 := NewTestCase("Case 2", "Do other things", "Case 1", []project.Label{},
		map[string]*project.Variant{}, duration.NewDuration(0, 3, 0), ProjectID)
	TestSeq, _ := NewTestSequence("Seq", "Do things", "boot", []project.Label{},
		[]Case{TestCase1, TestCase2}, ProjectID)
	TestSeqID := TestSeq.ID()
	SUTVersion := "1.0.1"
	SUTVariant := "Win 10"
	Time := duration.NewDuration(1, 2, 3)

	tsvID := id.NewTestVersionID(TestSeqID, tsvNr)
	TestSeqPrt, err := NewSequenceExecutionProtocol(protocolListerStub{}, tsvID, SUTVariant, SUTVersion, Time)
	if err != nil {
		t.Error(err)
	}

	if TestSeqPrt.TestVersion != tsvID {
		t.Errorf(SeqVersErrorMessage, TestSeqPrt.TestVersion, tsvID)
	}
	if TestSeqPrt.SUTVersion != SUTVersion {
		t.Errorf(SUTVersErrorMessage, TestSeqPrt.SUTVersion, SUTVersion)
	}
	if TestSeqPrt.SUTVariant != SUTVariant {
		t.Errorf(SUTVariantErrorMessage, TestSeqPrt.SUTVariant, SUTVariant)
	}
	if TestSeqPrt.OtherNeededTime != Time {
		t.Errorf(TimeErrorMessage, TestSeqPrt.OtherNeededTime, Time)
	}
}

func TestSequenceExecutionProtocol_Equals(t *testing.T) {
	a := defaultCaseProtocol.DeepCopy()
	stepprt11 := stepPrt1.DeepCopy()
	stepprt11.NeededTime = duration.NewDuration(0, 0, 0)

	b := CaseExecutionProtocol{
		TestVersion: id.NewTestVersionID(id.NewTestID(id.NewProjectID(id.NewActorID("owner"),
			"d8ed5f42"), "987654C", true), 1),
		SUTVersion: "Version 1.87",
		SUTVariant: "Unix",

		ExecutionDate: time.Date(2018, time.May, 10, 12, 12, 12, 12, time.UTC),
		StepProtocols: []StepExecutionProtocol{stepprt11, stepPrt2, stepPrt3, stepPrt4, stepPrt5, stepPrt6},
		Result:        Pass,
		Comment:       "Just minor fails",
	}
	c := a.DeepCopy()
	d := b.DeepCopy()

	seq1 := SequenceExecutionProtocol{
		TestVersion: id.NewTestVersionID(id.NewTestID(id.NewProjectID(id.NewActorID("owner"),
			"asf885sdf7"), "54dfg8sd", false), 1),
		SUTVersion:             "1.0.1",
		SUTVariant:             "Win 10",
		CaseExecutionProtocols: []id.ProtocolID{a.ID(), b.ID()},
	}
	seq2 := SequenceExecutionProtocol{
		TestVersion: id.NewTestVersionID(id.NewTestID(id.NewProjectID(id.NewActorID("owner"),
			"asf885sdf7"), "54dfg8sd", false), 1),
		SUTVersion:             "1.0.1",
		SUTVariant:             "Win 10",
		CaseExecutionProtocols: []id.ProtocolID{c.ID(), d.ID()},
	}
	if !seq1.Equals(seq2) {
		t.Errorf("seq1 is equal to seq2 but equals-function returned false")
	}

	seq2.CaseExecutionProtocols[0] = id.ProtocolID{}
	if seq1.Equals(seq2) {
		t.Errorf("seq1 isn't equal to seq2 but equals-function returned true. " +
			"(ProtocolID of first CaseProtocol in seq2 was changed)")
	}
	seq2.CaseExecutionProtocols = []id.ProtocolID{c.ID()}
	if seq1.Equals(seq2) {
		t.Errorf("seq1 isn't equal to seq2 but equals-function returned true. " +
			"(seq2 has only one entry but seq1 has two)")
	}

	seq2.CaseExecutionProtocols = nil
	if seq1.Equals(seq2) {
		t.Errorf("seq2 is nil and seq1 isn't so false was expected but equals-function returned true")
	}
}
