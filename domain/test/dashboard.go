/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"time"
)

type Dashboard struct {
	Project              *project.Project
	Cases                []*Case
	Variants             []*project.Variant
	ProtocolMap          map[id.TestID][]CaseExecutionProtocol
	DashboardElements    []DashboardElement
	DashboardElementTest DashboardElement
}

type DashboardElement struct {
	Variant  *project.Variant
	Versions []project.Version
	Results  []CaseResult
}

type CaseResult struct {
	TestCase *Case
	Results  []Result
}

func NewDashboard(proj *project.Project, cases []*Case, protocolMap map[id.TestID][]CaseExecutionProtocol) (Dashboard) {

	dashboardElements := make([]DashboardElement, 0)
	variants := make([]*project.Variant, 0)

	for _, variant := range proj.Variants {
		dashboardElements = append(dashboardElements, NewDashboardElement(cases, variant, protocolMap))
		variants = append(variants, variant)
	}

	var dashboardElement DashboardElement
	if len(dashboardElements) == 0 {
		dashboardElement = DashboardElement{}
	} else {
		dashboardElement = dashboardElements[0]
	}

	return Dashboard{
		Project:              proj,
		Cases:                cases,
		Variants:             variants,
		DashboardElements:    dashboardElements,
		ProtocolMap:          protocolMap,
		DashboardElementTest: dashboardElement,
	}
}

func NewDashboardElement(cases []*Case, variant *project.Variant, protocolMap map[id.TestID][]CaseExecutionProtocol) (DashboardElement) {

	results := GetResults(cases, protocolMap, variant)

	return DashboardElement{
		Variant:  variant,
		Versions: variant.Versions,
		Results:  results,
	}
}

// GetCaseResultForVariant returns the latest results for all versions of the protocols
func GetCaseResultForVariant(testCase *Case, protocols []CaseExecutionProtocol, variant *project.Variant) CaseResult {
	versions := variant.Versions
	results := make([]Result, 0)

	for _, version := range versions {
		if _, ok := testCase.TestCaseVersions[0].Variants[variant.Name]; ok &&
			containsVersion(testCase.TestCaseVersions[0].Variants[variant.Name].Versions, version) {
			results = append(results, getLatestResult(protocols, version))
		} else {
			results = append(results, NotApplicable)
		}
	}
	return CaseResult{
		TestCase: testCase,
		Results:  results,
	}

}

func getLatestResult(protocols []CaseExecutionProtocol, version project.Version) Result {
	var result Result
	var latest time.Time

	for _, protocol := range protocols {
		if protocol.SUTVersion == version.Name {
			if protocol.ExecutionDate.After(latest) {
				result = protocol.Result
				latest = protocol.ExecutionDate
			}
		}
	}

	return result
}

func GetResults(cases []*Case, protocolMap map[id.TestID][]CaseExecutionProtocol, variant *project.Variant) []CaseResult {
	results := make([]CaseResult, 0)
	// iterate over cases and append results where applicable
	for _, testCase := range cases {
		results = append(results, GetCaseResultForVariant(testCase, protocolMap[testCase.ID()], variant))
	}
	return results
}
