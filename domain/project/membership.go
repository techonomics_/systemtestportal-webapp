/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

// UserMembership contains information about the membership of
// a user in a project
type UserMembership struct {
	User        id.ActorID
	Role        RoleName
	MemberSince time.Time
}

// NewUserMembership creates a new membership with the user.
// The role of the member is set to tester by default.
// Returns a UserMembership.
func NewUserMembership(u id.ActorID) UserMembership {
	memberSince := time.Now().UTC().Round(time.Second)

	ms := UserMembership{
		User:        u,
		Role:        defaultRoles["Tester"].Name,
		MemberSince: memberSince,
	}
	return ms
}
