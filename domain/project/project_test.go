/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"

	"golang.org/x/crypto/openpgp/errors"
)

// TestNewProject tests the NewProject function
func TestNewProject(t *testing.T) {
	name := "TestProject"
	owner := id.NewActorID("testUser")
	desc := "TestDescription"
	vis := visibility.Public

	p := NewProject(name, owner, desc, vis)
	if p.Name != name {
		t.Errorf("Projectname = %s, want %s", p.Name, name)
	}
	if p.Description != desc {
		t.Errorf("Projectdescription = %s, want %s", p.Description, desc)
	}
	if p.Visibility != vis {
		t.Errorf("Projectvisibility = %d, want %d", p.Visibility, vis)
	}

	name = "BlankProject   "
	nameWant := "BlankProject"
	desc = "   BlankDescription"
	descWant := "BlankDescription"
	vis = visibility.Private

	p = NewProject(name, owner, desc, vis)
	if p.Name != nameWant {
		t.Errorf("Projectname = %s, want %s", p.Name, nameWant)
	}
	if p.Description != descWant {
		t.Errorf("Projectdescription = %s, want %s", p.Description, descWant)
	}
	if p.Visibility != vis {
		t.Errorf("Projectvisibility = %d, want %d", p.Visibility, vis)
	}
	if p.UserMembers == nil {
		t.Error("UserMembers was nil after initialization")
	}
}

func TestProject_ID(t *testing.T) {
	name := "TestProject"
	owner := id.NewActorID("testUser")
	p := NewProject(name, owner, "This is a project", visibility.Public)
	projectID := p.ID()
	if projectID != id.NewProjectID(p.Owner, name) {
		t.Errorf("Project-ID was not created correctly. Was %v+ but expected %v+", projectID, id.NewProjectID(p.Owner, name))
	}
}
func TestProject_Member(t *testing.T) {
	p := exampleProject()
	u := exampleUser()
	p.AddMember(u.ID())
	x := p.UserMembers[u.ID()]
	if x.User != u.ID() {
		t.Errorf("AddMember(%s) = \n%v \nwant \n%v", u, p.UserMembers[u.ID()], u)
	}

	p.RemoveMember(u)
	if _, ok := p.UserMembers[u.ID()]; ok {
		t.Errorf("RemoveMember(%s) = \n%v \nwant \n%v", u, p.UserMembers[u.ID()], nil)
	}
}

func TestProject_GetLabelByName(t *testing.T) {
	p := exampleProject()
	p.Labels = exampleLabels()

	label, err := p.GetLabelByName(p.Labels[0].Name)
	if p.Labels[0] != label {
		t.Errorf("Got wrong color by name, was %s expected %s", label, p.Labels[0])
	}

	_, err = p.GetLabelByName("This label doesn't exist")

	if err == nil {
		t.Errorf("Got no error for non existing label, was \"%v\" got \"%v\"", err, errors.InvalidArgumentError("Label doesn't exist in project"))
	}

	p.Labels = []Label{}

	_, err = p.GetLabelByName("No labels exist in the project")

	if err == nil {
		t.Errorf("Got no error for empty label array in project, was \"%v\" got \"%v\"", err, errors.InvalidArgumentError("Project contains no labels"))
	}
}

func exampleProject() Project {
	return Project{Name: "id",
		UserMembers: map[id.ActorID]UserMembership{}}
}

func exampleUser() *user.User {
	return &user.User{
		DisplayName:      "Alexander Kaiser",
		Name:             "alexanderkaiser",
		EMail:            "alexander.kaiser@gmx.de",
		RegistrationDate: time.Now().UTC().Round(time.Second)}
}

func exampleLabels() []Label {
	return []Label{
		NewLabel("TestLabel1", "TestDescription1", "ff0000"),
	}

}
