/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

// A RoleName is the name of a role
// in a project
type RoleName string

// A Role defines the permissions a user
// has in a project.
// It has a name and permissions.
type Role struct {
	Name        RoleName
	Permissions Permissions
}

// String returns the RoleName as string
func (r *RoleName) String() string {
	return string(*r)
}

// defaultRoles are the default roles that exist
// in a project when it is created.
// They are supervisor, manager and tester.
var defaultRoles = map[RoleName]*Role{
	"Supervisor": {
		Name: "Supervisor",
		Permissions: Permissions{
			ExecutionPermissions{
				Execute: true,
			},
			CasePermissions{
				CreateCase:    true,
				EditCase:      true,
				DeleteCase:    true,
				AssignCase:    true,
				DuplicateCase: true,
			},
			SequencePermissions{
				CreateSequence:    true,
				EditSequence:      true,
				DeleteSequence:    true,
				AssignSequence:    true,
				DuplicateSequence: true,
			},
			MemberPermissions{
				EditMembers: true,
			},
			SettingsPermissions{
				EditProject:     true,
				DeleteProject:   true,
				EditPermissions: true,
			},
		},
	},
	"Manager": {
		Name: "Manager",
		Permissions: Permissions{
			ExecutionPermissions{
				Execute: true,
			},
			CasePermissions{
				CreateCase:    true,
				EditCase:      true,
				DeleteCase:    true,
				AssignCase:    true,
				DuplicateCase: true,
			},
			SequencePermissions{
				CreateSequence: true,
				EditSequence:   true,
				DeleteSequence: true,
				AssignSequence: true,
			},
			MemberPermissions{
				EditMembers: true,
			},
			SettingsPermissions{
				EditProject:     false,
				DeleteProject:   false,
				EditPermissions: false,
			},
		},
	},
	"Tester": {
		Name: "Tester",
		Permissions: Permissions{
			ExecutionPermissions{
				Execute: true,
			},
			CasePermissions{
				CreateCase:    false,
				EditCase:      false,
				DeleteCase:    false,
				AssignCase:    false,
				DuplicateCase: false,
			},
			SequencePermissions{
				CreateSequence: false,
				EditSequence:   false,
				DeleteSequence: false,
				AssignSequence: false,
			},
			MemberPermissions{
				EditMembers: false,
			},
			SettingsPermissions{
				EditProject:     false,
				DeleteProject:   false,
				EditPermissions: false,
			},
		},
	},
}
