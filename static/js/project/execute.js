/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$.getScript("/static/js/util/ajax.js");
$.getScript("/static/js/util/common.js");

// Adds the listeners on page load (exectuted in comments.tmpl)
function initializeExecutionClickListener() {
    /* Button Assignment */
    $("#buttonExecuteFirstTestStep") .click(executeFirstTestStep);
    $("#buttonExecuteNextTestStep")  .click(executeNextTestStep);
    $("#buttonSummaryFinish")        .click(finishSummary);
    $("#buttonAbort")                .click(backToTestObject);
    $("#buttonPause")                .click(pauseTimer);
}

/* Start timer */
var timerID;
var secondsElement;
var minutesElement;
var hoursElement;
$(window).load(initTimer());

function initTimer(){
    // formatting
    secondsElement = $('#timeSeconds');
    minutesElement = $('#timeMinutes');
    hoursElement = $('#timeHours');
    var sec = parseInt(secondsElement.text());
    var min = parseInt(minutesElement.text());
    var hour = parseInt(hoursElement.text());
    if (isNaN(sec)) sec = 0;
    if (isNaN(min)) min = 0;
    if (isNaN(hour)) hour = 0;
    secondsElement.text(sec.toLocaleString('en-US',{minimumIntegerDigits: 2, useGrouping:false}));
    minutesElement.text(min.toLocaleString('en-US',{minimumIntegerDigits: 2, useGrouping:false}));
    hoursElement.text(hour.toString());

    startTimer();
}

/* pauses the timer  */
function pauseTimer() {
    clearInterval(timerID);
    var pauseButton = $("#buttonPause");
    pauseButton.off();
    pauseButton.click(startTimer);
    $("#buttonPauseIcon").removeClass("fa-pause fa-play").addClass("fa-play")
}

/* resumes the timer  */
function startTimer() {
    clearInterval(timerID);
    timerID = setInterval(tickSec, 1000);
    var pauseButton = $("#buttonPause");
    pauseButton.off();
    pauseButton.click(pauseTimer);
    $("#buttonPauseIcon").removeClass("fa-pause fa-play").addClass("fa-pause")
}

/* steps back to the test object  */
function backToTestObject(event) {
    var requestURL = getTestURL().toString();
    ajaxRequestFragment(event, requestURL, "", "GET");
}

/* saves inputs and requests for first step execution page */
function executeFirstTestStep(event) {
    ajaxRequestFragment(event, currentURL().toString(), getExecutionStartPageData(), "POST");
}

/* saves inputs and requests for next step execution page */
function executeNextTestStep(event) {
    ajaxRequestFragment(event, currentURL().toString(), getExecutionStepPageData(), "POST");
}
/* saves inputs and redirect */
function finishSummary(event) {
    var path = currentURL().removeLastSegments(1).toString();
    var data = getSummaryPageData();
    if (data.case === "0" || data.case ==="-1"){
        ajaxRequestFragmentWithHistory(event, currentURL().toString(), data, "POST",path);
    }else{
        ajaxRequestFragment(event, currentURL().toString(), data, "POST");
    }
}

/* helper */

// tickSec shows next second on the timer
function tickSec(){
    var sec = parseInt(secondsElement.text());
    sec++;
    if (sec >= 60){
        sec -= 60;
        tickMin()
    }
    secondsElement.text(sec.toLocaleString('en-US',{minimumIntegerDigits: 2, useGrouping:false}));
}
// tickMin shows next minute on the timer
function tickMin(){
    var min = parseInt(minutesElement.text());
    min++;
    if (min >= 60){
        min -= 60;
        tickHour();
    }
    minutesElement.text(min.toLocaleString('en-US',{minimumIntegerDigits: 2, useGrouping:false}));
}
// tickHour shows next hour on the timer
function tickHour(){
    var hour = parseInt(hoursElement.text());
    hour++;
    hoursElement.text(hour.toString());
}

// getExecutionStartPageData returns an array with the information given on the execution start page
function getExecutionStartPageData() {
    return {
        fragment : true,
        step: 0,
        case: $('#inputTestCaseNumber').val(),
        inputSUTVariant: $('#inputTestObjectSUTVariants').val(),
        inputSUTVersion: $('#inputTestObjectSUTVersions').val(),
        seconds: $('#timeSeconds').text(),
        minutes: $('#timeMinutes').text(),
        hours: $('#timeHours').text()
    }
}

// getExecutionStartPageData returns an array with the information given on the execution step page
function getExecutionStepPageData() {
    return {
        fragment : true,
        step: $('#inputTestStepNumber').val(),
        case: $('#inputTestCaseNumber').val(),
        result: $("input:radio[name ='testResults']:checked").val(),
        notes: $('#inputTestStepNotes').val(),
        inputTestStepActualResult: $('#inputTestStepActualResult').val(),
        seconds: $('#timeSeconds').text(),
        minutes: $('#timeMinutes').text(),
        hours: $('#timeHours').text()
    }
}
// getSummaryPageData returns an array with the information given on the summary page
function getSummaryPageData() {
    return {
        fragment : true,
        step: $('#inputTestStepNumber').val(),
        case: $('#inputTestCaseNumber').val(),
        notes: $('#inputTestComment').val(),
        result: $("input:radio[name ='testResults']:checked").val(),
        seconds: $('#timeSeconds').text(),
        minutes: $('#timeMinutes').text(),
        hours: $('#timeHours').text()
    }
}