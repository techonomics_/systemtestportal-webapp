/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

// getFormValueInt gets the value to the given key from the request and converts it into an int.
// Returns -2 if the value empty.
// Returns -1 if the value is not an integer.
func getFormValueInt(r *http.Request, key string) int {
	ver := r.FormValue(key)
	if ver == "" {
		return -2
	}
	return handler.StringToInt(ver)
}

// getFormValueString gets the value to the given key from the request.
// Returns empty string if the key does not exists.
func getFormValueString(r *http.Request, key string) string {
	return r.FormValue(key)
}

// getFormValueInt gets the value to the given key from the request and converts it into a test.result.
// ok is true, if the string could be matched to a result, false otherwise
func getFormValueResult(r *http.Request, key string) (result test.Result, ok bool) {
	ver := r.FormValue(key)
	return stringToResult(ver)
}

// stringToResult gets a string and converts it to a test.result
// ok is true, if the string could be matched to a result, false otherwise
func stringToResult(s string) (result test.Result, ok bool) {
	switch s {
	case "0", "NotAssessed":
		return test.NotAssessed, true
	case "1", "Pass":
		return test.Pass, true
	case "2", "PassWithComment":
		return test.PassWithComment, true
	case "3", "Fail":
		return test.Fail, true
	default:
		return test.NotAssessed, false
	}
}
