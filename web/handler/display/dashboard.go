package display

import (
	"log"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"net/http"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/store/protocol"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"sort"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
	"html/template"
)

// DashboardGet supplies a handler to display the dashboard page
func DashboardGet(l handler.TestCaseProtocolLister, caseLister handler.TestCaseLister) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		tmpl := getProjectDashboardFragment(request)
		ctxtEntities := handler.GetContextEntities(request)
		// Get the Project
		if ctxtEntities.Project == nil {
			errors.Handle(ctxtEntities.Err, writer, request)
			return
		}
		project := GetProject(ctxtEntities, writer, request)

		caseProtocolList := GetCaseProtocols(project, writer, request)

		cases := GetCases(caseLister, project)

		protocolMap := GetProtocolMap(caseProtocolList, l)

		dashboard := test.NewDashboard(project, cases, protocolMap)

		// Fill the Datamap
		contexts := context.New().
			WithUserInformation(request).
			With(context.Project, project).
			With(context.Dashboard, dashboard)

		handler.PrintTmpl(contexts, tmpl, writer, request)
	}
}

// GetProject returns the current project
func GetProject(ctxtEntities *handler.ContextEntities, writer http.ResponseWriter, request *http.Request) (*project.Project) {
	project, wasFound, err := store.GetProjectStore().Get(ctxtEntities.Project.ID())
	if !wasFound || err != nil {
		errors.Handle(err, writer, request)
	}
	return project
}

//GetCases returns all cases of the project
func GetCases(caseLister handler.TestCaseLister, project *project.Project) []*test.Case {
	cases, err := caseLister.List(project.ID())
	if err != nil {
		log.Println(err)
	}
	return cases
}

// GetProtocolMap returns a map of test id's and execution protocols
func GetProtocolMap(caseProtocolList []test.CaseExecutionProtocol, l handler.TestCaseProtocolLister) map[id.TestID][]test.CaseExecutionProtocol {
	protocolMap := make(map[id.TestID][]test.CaseExecutionProtocol)
	for _, caseProtocol := range caseProtocolList {
		if caseProtocol.TestVersion.IsCase() {
			protocolMap[caseProtocol.TestVersion.TestID], _ = l.GetCaseExecutionProtocols(caseProtocol.TestVersion.TestID)
		}
	}
	return protocolMap
}

// GetCaseProtocols returns test case protocols of the project
func GetCaseProtocols(project *project.Project, writer http.ResponseWriter, request *http.Request) []test.CaseExecutionProtocol {
	caseProtocolList, err := protocol.GetProtocolStore().GetCaseExecutionProtocolsForProject(project.ID())
	if err != nil {
		errors.Handle(err, writer, request)
	}
	return caseProtocolList
}

// GetKeys returns an sorted string array that contains all names of variants in the project
func GetKeys(project *project.Project) []string {
	keys := make([]string, len(project.Variants))
	i := 0
	for k := range project.Variants {
		keys[i] = k
		i++
	}
	// sort keys so we always get the same variant
	sort.Strings(keys)
	return keys
}

func getProjectDashboardFragment(request *http.Request) *template.Template {
	if httputil.IsFragmentRequest(request) {
		return getTabDashboardFragment()
	}
	return getTabDashboardTree()
}

func getTabDashboardTree() *template.Template {
	return handler.GetNoSideBarTree().
		Append(templates.ContentProjectTabs).
		Append(templates.Dashboard).
		Get().Lookup(templates.HeaderDef)
}

func getTabDashboardFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.Dashboard).
		Get().Lookup(templates.TabContent)
}
