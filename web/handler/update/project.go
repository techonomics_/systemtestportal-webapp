/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// The error-messages for updating a project
const (
	errCanNotUpdateProjectTitle = "Couldn't update project."
	errCanNotUpdateProject      = "We are sorry but we were unable to update the project as you requested." +
		"If you believe this is a bug please contact us via our " + handler.IssueTracker + "."
)

// ProjectPost is used to update an existing project
func ProjectPost(pa handler.ProjectAdder, pec id.ProjectExistenceChecker) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		// Owner always has the right to edit the settings. This prevents the members to
		// lock themselves out from editing the project and the roles
		if !c.Project.IsOwner(c.User) && !c.Project.GetPermissions(c.User).EditProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		// pn := r.FormValue(httputil.ProjectName)
		pd := r.FormValue(httputil.ProjectDescription)
		pvs := r.FormValue(httputil.ProjectVisibility)
		pv, err := visibility.StringToVis(pvs)

		if err != nil {
			errors.Handle(handler.InvalidVisibility(), w, r)
			return
		}

		// changes to the name of a project should be validated
		// (*p).Name = pn
		(*c.Project).Description = pd
		(*c.Project).Visibility = pv

		if err := pa.Add(c.Project); err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				errCanNotUpdateProjectTitle, errCanNotUpdateProject, r).
				WithLog("Unable to update project").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		w.Header().Set(httputil.NewName, c.Project.Name)
	}
}
