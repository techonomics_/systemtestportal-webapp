/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package json

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCaseProtocolsGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseProtocolListerMock{}
				return CaseProtocolsGet(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(m, 0),
				)
			},
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Protocol lister returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseProtocolListerMock{Err: handler.ErrTest}
				return CaseProtocolsGet(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(m, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Empty protocol list",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseProtocolListerMock{}
				return CaseProtocolsGet(m), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(m, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseProtocolListerMock{Protocols: []test.CaseExecutionProtocol{{}, {}}}
				return CaseProtocolsGet(m), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(m, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestSequenceProtocolsGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.SequenceProtocolListerMock{}
				return SequenceProtocolsGet(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(m, 0),
				)
			},
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Protocol lister returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.SequenceProtocolListerMock{Err: handler.ErrTest}
				return SequenceProtocolsGet(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(m, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Empty protocol list",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.SequenceProtocolListerMock{}
				return SequenceProtocolsGet(m), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(m, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.SequenceProtocolListerMock{Protocols: []test.SequenceExecutionProtocol{{}, {}}}
				return SequenceProtocolsGet(m), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(m, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}
