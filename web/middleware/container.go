/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package middleware

import (
	"net/http"

	"fmt"

	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

// ContainerKey is used to retrieve the container out of the
// request context. This can either be a user or a group.
const ContainerKey = "container"

const (
	nonExistentContainerTitle = "Requested container doesn't exist!"
	nonExistentContainer      = "You requested a container(group or user) " +
		"which doesn't exist anymore or never existed."
)

// GroupRetriever is used to get a group by name.
type GroupRetriever interface {

	// Get returns the group with the given name
	Get(groupID id.ActorID) (*group.Group, bool, error)
}

// UserRetriever is used to get a user by id.
type UserRetriever interface {

	// Get returns the user with the given ID
	Get(userID id.ActorID) (*user.User, bool, error)
}

// Container returns a middleware that will get the container for a project
// from a request. For this to work
func Container(user UserRetriever, group GroupRetriever) negroni.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		cName, err := getParam(r, ContainerKey)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		cID := id.NewActorID(cName)

		// Try user
		u, ok, err := user.Get(cID)
		if ok {
			AddToContext(r, ContainerKey, u)
			next(w, r)
			return
		} else if err != nil {
			errors.Respond(internalError(fmt.Sprintf("Unable to load "+
				"container with id: '%s'.", cID), err, r), w)
			return
		}

		// Try group
		g, ok, err := group.Get(cID)
		if ok {
			AddToContext(r, ContainerKey, g)
			next(w, r)
			return
		} else if err != nil {
			errors.Respond(internalError(fmt.Sprintf("Unable to load "+
				"container with id: '%s'.", cID), err, r), w)
			return
		}

		// Manage errors
		errors.ConstructStd(http.StatusBadRequest, nonExistentContainerTitle, nonExistentContainer, r).
			WithLogf("Client request non-existent container '%s'.", cID).
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
	}
}
