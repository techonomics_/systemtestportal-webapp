/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package sessions

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

func TestSessionStore_GetCurrent_Negative(t *testing.T) {
	InitSessionManagement(nil, nil)
	store := GetSessionStore()
	resultUser, err := store.GetCurrent(httptest.NewRequest(http.MethodGet, "http://example.com/foo", nil))
	if resultUser != nil {
		t.Errorf("Returned %+v but expected none.", resultUser)
	}
	if err != nil {
		t.Error(err)
	}
}

func TestSessionStore_StartFor(t *testing.T) {
	testUser := user.New("TestDisplay", "TestAccount", "test@mail.com")
	storeStub := TestStore{nil}
	InitSessionManagement(&storeStub, nil)

	store := GetSessionStore()
	req := httptest.NewRequest(http.MethodGet, "http://example.com/foo", nil)
	rec := httptest.NewRecorder()
	err := store.StartFor(rec, req, &testUser)

	if err != nil {
		t.Error(err)
	}
	if storeStub.session == nil {
		t.Errorf("Expected new session but was %+v.", storeStub.session)
	}
	if storeStub.session.Name() != store.getUserSessionName() {
		t.Errorf("New session has wrong name. Expected %s but was %s.", store.getUserSessionName(), storeStub.session.Name())
	}
	if id, ok := storeStub.session.Values[idKey]; !ok || id != testUser.ID() {
		t.Errorf("ID wasn't saved correctly. Expected %s but was %s.", testUser.ID(), id)
	}
}

func TestSessionStore_GetCurrent(t *testing.T) {
	testUser := user.New("TestDisplay", "TestAccount", "test@mail.com")
	storeStub := TestStore{nil}
	userServerStub := testUserServer{testUser}
	InitSessionManagement(&storeStub, &userServerStub)

	store := GetSessionStore()
	req := httptest.NewRequest(http.MethodGet, "http://example.com/foo", nil)
	rec := httptest.NewRecorder()
	err1 := store.StartFor(rec, req, &testUser)
	resultUser, err2 := store.GetCurrent(req)

	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Error(err2)
	}
	if *resultUser != testUser {
		t.Errorf("Wrong user was read. Expected %+v but was %+v.", testUser, resultUser)
	}
}

func TestSessionStore_EndFor(t *testing.T) {
	testUser := user.New("TestDisplay", "TestAccount", "test@mail.com")
	storeStub := TestStore{nil}
	userServerStub := testUserServer{testUser}
	InitSessionManagement(&storeStub, &userServerStub)

	store := GetSessionStore()
	req := httptest.NewRequest(http.MethodGet, "http://example.com/foo", nil)
	rec := httptest.NewRecorder()
	err1 := store.StartFor(rec, req, &testUser)
	err2 := store.EndFor(rec, req, nil)

	resultUser, err3 := store.GetCurrent(req)

	if resultUser != nil {
		t.Errorf("Found user %+v but expected none.", resultUser)
	}
	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Error(err2)
	}
	if err3 != nil {
		t.Error(err3)
	}
}

//testUserServer is a Test-Stub of RegisterServer
type testUserServer struct {
	savedUser user.User
}

func (tus *testUserServer) Get(id id.ActorID) (*user.User, bool, error) {
	return &tus.savedUser, true, nil
}
