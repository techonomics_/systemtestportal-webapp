/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"net/http"

	"github.com/dimfeld/httptreemux"
	domainProject "gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
)

/*
-----  Helpers  -----
*/

func allMethods(r *httptreemux.ContextMux, path string, handler http.Handler) {
	r.Handler(http.MethodGet, path, handler)
	r.Handler(http.MethodDelete, path, handler)
	r.Handler(http.MethodPost, path, handler)
	r.Handler(http.MethodPut, path, handler)
}

func wrapContextGroup(cg *httptreemux.ContextGroup) *contextGroup {
	return &contextGroup{cg}
}

type contextGroup struct {
	*httptreemux.ContextGroup
}

func (g *contextGroup) HandlerGet(path string, handler http.Handler) {
	g.Handler(http.MethodGet, path, handler)
}

func (g *contextGroup) HandlerPost(path string, handler http.Handler) {
	g.Handler(http.MethodPost, path, handler)
}

func (g *contextGroup) HandlerPut(path string, handler http.Handler) {
	g.Handler(http.MethodPut, path, handler)
}

func (g *contextGroup) HandlerDelete(path string, handler http.Handler) {
	g.Handler(http.MethodDelete, path, handler)
}

/*
-----  Temporary stubs  -----
*/
type wrapProjectStore struct {
	store.Projects
}

func (w *wrapProjectStore) List() ([]*domainProject.Project, error) {
	return w.Projects.ListAll()
}
