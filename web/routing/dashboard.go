package routing

import (
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
)

func registerDashboardHandler(pg *contextGroup, n *negroni.Negroni, tcStore store.Cases,
	protocolLister handler.TestCaseProtocolLister) {

	prtg := wrapContextGroup(pg.NewContextGroup(Dashboard))

	prtg.HandlerGet(Show,
		n.With(negroni.WrapFunc(display.DashboardGet(protocolLister, tcStore))))

}
