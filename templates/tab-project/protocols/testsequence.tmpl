{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div class="tab-card card" id="tabTestProtocols">
    <nav class="navbar navbar-light action-bar p-3">
        <div class="input-group flex-nowrap">
            <button class="btn btn-secondary mr-2" id="buttonBack">
                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                <span class="d-none d-sm-inline"> Back</span>
            </button>
        </div>
    </nav>
    <div class="row tab-side-bar-row">
        <div class="col-md-9 p-3">
            <h4 class="mb-3">
                <span id="contentTestSequenceResult" class="text-muted"></span>
                <span id="protocolName">Protocol</span>
            </h4>
            <div id="contentTestCaseNotesContainer" class="form-group d-none">
                <label><strong>Notes</strong></label>
                <p id="contentTestCaseNotes" class="text-muted">
                    No Notes
                </p>
            </div>
            <div class="form-group">
                <label><strong>Test Sequence Description</strong></label>
                <p id="contentTestSequenceDescription" class="text-muted">
                    No Description
                </p>
            </div>
            <div class="form-group">
                <label><strong>Test Sequence Preconditions</strong></label>
                <p id="contentTestSequencePreconditions" class="text-muted">
                    No Conditions
                </p>
            </div>
            <div class="form-group">
                <label><strong>Test Case Results</strong></label>
            </div>
            <table class="table table-hover table-responsive">
                <thead>
                <tr>
                    <th style="width:10%;">Result</th>
                    <th style="width:90%;">Test Case</th>
                    <!--<th style="width:40%;">Time</th>-->
                </tr>
                </thead>
                <tbody id="protocolTable">
                <tr>
                    <td colspan="2" class="text-center">... Loading ...</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-3 p-3 tab-side-bar">
            <div class="form-row">
                <div class="col-12">
                    <strong>Execution Date</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestSequenceExecutionDate" class="text-muted">

                    </p>
                </div>
                <div class="col-12">
                    <strong>Tester</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestSequenceTester" class="text-muted">
                        some user
                    </p>
                </div>
                <div class="col-12">
                    <strong>System Variant</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestSequenceSUTVariant" class="text-muted">
                        some variant
                    </p>
                </div>
                <div class="col-12">
                    <strong>System Version</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestSequenceSUTVersion" class="text-muted">
                        some system version
                    </p>
                </div>
                <div class="col-12">
                    <strong>Sequence Version</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestSequenceVersion" class="text-muted">
                        some version
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/project/testprotocols.js"></script>
    <script>
        $( document ).ready(function() {
            fillTestSequenceResult();
            jQuery("time.timeago").timeago();
        });

        $("#printerIcon").removeClass("d-none");

        function printer() {
            printpr();
        }
    </script>
</div>
{{end}}

