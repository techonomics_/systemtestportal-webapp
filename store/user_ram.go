// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build ram

package store

import (
	"sync"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"golang.org/x/crypto/bcrypt"
)

//usersRAM provides functions for user-handling like add, get or validate
type usersRAM struct {
	sync.RWMutex
	// list is a map of all users in the system. It maps the name of an user to the user itself.
	list map[string]*user.User
	// emails is a map of all user emails in the system. It maps emails to user IDs.
	emails map[string]string
	// passwords is a map of all user password in the system. A unique user name is mapped to the password
	passwords map[string]string
}

func initUsersRAM() *usersRAM {
	s := usersRAM{
		list:      make(map[string]*user.User),
		emails:    make(map[string]string),
		passwords: make(map[string]string),
	}

	return &s
}

//Add adds an user to the user-database
//If user already exists, function overrides the existing one
func (s *usersRAM) Add(pur *user.PasswordUser) error {
	if pur == nil {
		return nil
	}

	s.Lock()
	defer s.Unlock()

	// Encrypt the users password and overwrite the plaintext in the given user
	pwHash, err := bcrypt.GenerateFromPassword([]byte(pur.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	pur.Password = string(pwHash)

	// Save the user in the database
	s.emails[pur.EMail] = pur.Name
	s.list[pur.Name] = &pur.User
	s.passwords[pur.Name] = pur.Password
	return nil
}

//Get searches for an user with the given id
func (s *usersRAM) Get(id id.ActorID) (*user.User, bool, error) {
	s.RLock()
	defer s.RUnlock()

	result, found := s.list[id.Actor()]

	return result, found, nil
}

//Get searches for an user with the given email
func (s *usersRAM) GetByMail(email string) (*user.User, bool, error) {
	s.RLock()
	defer s.RUnlock()

	uID, ok := s.emails[email]
	if ok {
		result, found := s.list[uID]
		return result, found, nil
	}

	return nil, false, nil
}

//List returns a slice containing all user profiles
func (s *usersRAM) List() ([]*user.User, error) {
	s.RLock()
	defer s.RUnlock()

	var l []*user.User
	for _, u := range s.list {
		l = append(l, u)
	}

	return l, nil
}

// getPassword gets the password of a user.
// Returns the password as string and true or an empty string
// and false if the user does not exist.
func (s *usersRAM) getPassword(u *user.User) (string, bool) {
	s.RLock()
	defer s.RUnlock()

	password, ok := s.passwords[u.Name]
	if !ok {
		return "", false
	}

	return password, true
}

// Validate authenticates a user. The identifier is either a name or an email.
// returns identified user, a bool if validations was successful and error if any occurred
func (s *usersRAM) Validate(identifier string, password string) (*user.User, bool, error) {
	s.RLock()
	defer s.RUnlock()

	u, ok, err := s.Get(id.NewActorID(identifier))
	if err != nil {
		return nil, false, err
	}

	if !ok {
		u, ok, err = s.GetByMail(identifier)
	}

	if err != nil || !ok {
		return nil, false, err
	}

	// Retrieve the users password
	up, ok := s.getPassword(u)
	if !ok {
		return nil, false, nil
	}

	// Create a user with a password
	pur := user.PasswordUser{
		Password: up,
		User:     *u,
	}

	// The user pur contains the hashed password from the database
	// Compare plain text password from input and hashed password from database
	err = bcrypt.CompareHashAndPassword([]byte(pur.Password), []byte(password))
	if err != nil {
		if err == bcrypt.ErrMismatchedHashAndPassword {
			return nil, false, nil
		}
		return nil, false, err
	}

	return u, u.Name == identifier || u.EMail == identifier, nil
}

func (s *usersRAM) GetAssignments(u *user.User) []id.TestID {
	s.RLock()
	defer s.RUnlock()

	if u == nil {
		return nil
	}

	projects, err := projectStore.ListAll()
	if err != nil {
		return nil
	}

	var t []id.TestID
	for _, p := range projects {
		if _, ok := p.UserMembers[u.ID()]; !ok {
			continue
		}

		pt, err := listCaseAssignments(u, p)
		if err != nil {
			return nil
		}

		t = append(t, pt...)

		pt, err = listSequenceAssignments(u, p)
		if err != nil {
			return nil
		}

		t = append(t, pt...)
	}

	return t
}

func listCaseAssignments(u *user.User, p *project.Project) ([]id.TestID, error) {
	cases, err := caseStore.List(p.ID())
	if err != nil {
		return nil, err
	}

	var t []id.TestID
	for _, tc := range cases {
		_, ok := tc.TestCaseVersions[0].Tester[u.Name]
		if ok {
			t = append(t, tc.ID())
		}
	}

	return t, nil
}

func listSequenceAssignments(u *user.User, p *project.Project) ([]id.TestID, error) {
	sequences, err := sequenceStore.List(p.ID())
	if err != nil {
		return nil, err
	}

	var t []id.TestID
	for _, ts := range sequences {
		_, ok := ts.SequenceVersions[0].Tester[u.Name]
		if ok {
			t = append(t, ts.ID())
		}
	}

	return t, nil
}
