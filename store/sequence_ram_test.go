// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build ram

package store

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

var testSequenceStore = &sequencesRAM{
	sequences: map[string]map[string]map[string]*test.Sequence{
		"user-1": {
			"p0001": {
				"test-sequence-1": {
					Name: "Test Sequence 1",
				},
				"test-sequence-2": {
					Name: "Test Sequence 2",
				},
			},
			"awesome-project": {
				"create-tests": {
					Name: "Create tests",
				},
				"assignment-tests": {
					Name: "Assign tests",
				},
			},
			"another-project": {
				"Execute test sequence": {
					Name: "Anöther Project",
				},
			},
		},
	},
}

var _ Sequences = initSequencesRAM()

func TestSequencesRAMNilMethodCalls(t *testing.T) {
	var empty *sequencesRAM
	t.Run("List", func(t *testing.T) {
		defer expectPanic(t)
		empty.List(id.NewProjectID("user-1", "awesome-project"))
	})
	t.Run("Add (nil store)", func(t *testing.T) {
		defer expectPanic(t)
		empty.Add(&test.Sequence{Name: "login-2", Project: id.NewProjectID("user-1", "awesome-project")})
	})
	t.Run("Add (nil test sequence)", func(t *testing.T) {
		defer expectPanic(t)
		initSequencesRAM().Add(nil)
	})
	t.Run("Get", func(t *testing.T) {
		defer expectPanic(t)
		empty.Get(id.NewTestID(id.NewProjectID("user-1", "awesome-project"), "login", false))
	})
	t.Run("Delete", func(t *testing.T) {
		defer expectPanic(t)
		empty.Delete(id.NewTestID(id.NewProjectID("user-1", "awesome-project"), "login", false))
	})
}

func TestSequencesRAMList(t *testing.T) {
	type args struct {
		pID id.ProjectID
	}
	tests := []struct {
		name string
		s    *sequencesRAM
		args args
		want []*test.Sequence
	}{
		{
			name: "Unknown container",
			s:    testSequenceStore,
			args: args{
				pID: id.NewProjectID("user-2", "p0001"),
			},
			want: nil,
		},
		{
			name: "Unknown project",
			s:    testSequenceStore,
			args: args{
				pID: id.NewProjectID("user-1", "unknown"),
			},
			want: nil,
		},
		{
			name: "Known container and project",
			s:    testSequenceStore,
			args: args{
				pID: id.NewProjectID("user-1", "awesome-project"),
			},
			want: []*test.Sequence{
				testSequenceStore.sequences["user-1"]["awesome-project"]["create-tests"],
				testSequenceStore.sequences["user-1"]["awesome-project"]["assignment-tests"],
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := tt.s.List(tt.args.pID); !containsSequencesUnordered(got, tt.want) {
				t.Errorf("caseRAMStore.List() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSequencesRAMAdd(t *testing.T) {
	type args struct {
		cID string
		pID string
		ts  *test.Sequence
	}
	tests := []struct {
		name string
		s    *sequencesRAM
		args args
		want error
	}{
		{
			name: "New container",
			s:    initSequencesRAM(),
			args: args{
				ts: &test.Sequence{
					Name:    "Cool Test Case",
					Project: id.NewProjectID("user-1", "cool-project"),
				},
			},
			want: nil,
		},
		{
			name: "New project",
			s: &sequencesRAM{
				sequences: map[string]map[string]map[string]*test.Sequence{
					"user-1": {},
				},
			},
			args: args{
				cID: "user-1",
				pID: "cool-project",
				ts: &test.Sequence{
					Name: "Cool Test Case",
				},
			},
			want: nil,
		},
		{
			name: "Existing container and project",
			s: &sequencesRAM{
				sequences: map[string]map[string]map[string]*test.Sequence{
					"user-1": {
						"cool-project": {},
					},
				},
			},
			args: args{
				cID: "user-1",
				pID: "cool-project",
				ts: &test.Sequence{
					Name: "Cool Test Case",
				},
			},
			want: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.Add(tt.args.ts); got != tt.want {
				t.Errorf("caseRAMStore.Add() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSequencesRAMGet(t *testing.T) {
	type args struct {
		tsID id.TestID
	}
	type want struct {
		ts *test.Sequence
		ok bool
	}
	tests := []struct {
		name string
		s    *sequencesRAM
		args args
		want want
	}{
		{
			name: "Unknown container",
			s:    testSequenceStore,
			args: args{
				tsID: id.NewTestID(id.NewProjectID("user-2", "p0001"), "test-case-2", false),
			},
			want: want{
				ts: nil,
				ok: false,
			},
		},
		{
			name: "Unknown project",
			s:    testSequenceStore,
			args: args{
				tsID: id.NewTestID(id.NewProjectID("user-1", "p0002"), "test-case-2", false),
			},
			want: want{
				ts: nil,
				ok: false,
			},
		},
		{
			name: "Unknown test case",
			s:    testSequenceStore,
			args: args{
				tsID: id.NewTestID(id.NewProjectID("user-1", "p0001"), "test-case-3", false),
			},
			want: want{
				ts: nil,
				ok: false,
			},
		},
		{
			name: "Known test sequence",
			s:    testSequenceStore,
			args: args{
				tsID: id.NewTestID(id.NewProjectID("user-1", "p0001"), "test-sequence-2", false),
			},
			want: want{
				ts: testSequenceStore.sequences["user-1"]["p0001"]["test-sequence-2"],
				ok: true,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTC, gotOK, gotError := tt.s.Get(tt.args.tsID)
			if gotTC != tt.want.ts || gotOK != tt.want.ok || gotError != nil {
				t.Errorf("Get( %v) = (%v, %v, %v), want (%v, %v, nil)", tt.args.tsID,
					gotTC, gotOK, gotError, tt.want.ts, tt.want.ok)
			}

		})
	}
}

func TestSequencesRAMDelete(t *testing.T) {
	type args struct {
		tsID id.TestID
	}
	tests := []struct {
		name string
		s    *sequencesRAM
		args args
	}{
		{
			name: "Empty store",
			s:    initSequencesRAM(),
			args: args{
				tsID: id.NewTestID(id.NewProjectID("user-1", "p0001"), "test-case-1", false),
			},
		},
		{
			name: "Unknown user",
			s:    testSequenceStore,
			args: args{
				tsID: id.NewTestID(id.NewProjectID("user-53", "p0001"), "test-case-1", false),
			},
		},
		{
			name: "Unknown project",
			s:    testSequenceStore,
			args: args{
				tsID: id.NewTestID(id.NewProjectID("user-1", "p0002"), "test-case-1", false),
			},
		},
		{
			name: "Unknown test case",
			s:    testSequenceStore,
			args: args{
				tsID: id.NewTestID(id.NewProjectID("user-1", "p0001"), "test-case-2", false),
			},
		},
		{
			name: "Known test case",
			s:    testSequenceStore,
			args: args{
				tsID: id.NewTestID(id.NewProjectID("user-1", "p0001"), "test-case-1", false),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.s.Delete(tt.args.tsID)

			if _, found, _ := tt.s.Get(tt.args.tsID); found {
				t.Errorf("Test case was not deleted")
			}
		})
	}
}

func TestSequencesRAMAddAndGet(t *testing.T) {
	type args struct {
		ts *test.Sequence
	}
	tests := []struct {
		name string
		s    *sequencesRAM
		args args
	}{
		{
			name: "Basic example",
			s:    initSequencesRAM(),
			args: args{
				ts: &test.Sequence{
					Name:    "New Test Case",
					Project: id.NewProjectID("user-1", "p0001"),
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotError := tt.s.Add(tt.args.ts)

			if gotError != nil {
				t.Errorf("Add(%v) = %v, want nil", tt.args.ts, gotError)
			}

			gotTC, gotOK, gotError := tt.s.Get(tt.args.ts.ID())

			if !gotOK || gotTC != tt.args.ts || gotError != nil {
				t.Errorf("Get(%+v) = (%v, %v, %v), want (%v, %v, nil)",
					tt.args.ts.ID(), gotTC, gotOK, gotError, tt.args.ts, true)
			}
		})
	}
}

func containsSequencesUnordered(got, want []*test.Sequence) bool {
outer:
	for _, w := range want {
		for _, g := range got {
			if w == g {
				continue outer
			}
		}
		return false
	}
	return true
}
