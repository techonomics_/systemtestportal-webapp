-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO test_sequence_versions (
    id, test_sequence_id, version_nr, description, preconditions, message, is_minor
) VALUES
    (1, 1, 1, 'Test execution with different types of test suite', '', 'Initial version', 0),
    (2, 1, 2, 'Execution of different types of test suites', '', 'Add missing test case', 0),
    (3, 2, 1, 'A test sequence containing a deleted test case', '', 'Initial version', 0);

-- +migrate Down
DELETE FROM test_sequence_versions;