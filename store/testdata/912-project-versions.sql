-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO project_versions (id, variant_id, name)
VALUES
    ( 1,  1, 'v0.1.0-win-x86'),
    ( 2,  2, 'v0.1.0-win-amd64'),
    ( 3,  3, 'v0.1.0-linux-x86'),
    ( 4,  4, 'v0.1.0-linux-amd64'),
    ( 5,  5, 'Standard'),
    ( 6,  8, 'Standard'),
    ( 7,  1, 'v0.1.1-win-x86'),
    ( 8,  2, 'v0.1.1-win-amd64'),
    ( 9,  3, 'v0.1.1-linux-x86'),
    (10,  4, 'v0.1.1-linux-amd64'),
    (11,  1, 'v0.1.2-win-x86'),
    (12,  2, 'v0.1.2-win-amd64'),
    (13,  3, 'v0.1.2-linux-x86'),
    (14,  4, 'v0.1.2-linux-amd64'),
    (15,  6, 'v1.0.0-x86'),
    (16,  7, 'v1.0.0-x86'),
    (17,  9, 'v1.0.0-x86'),
    (18, 10, 'v1.0.0-x86'),
    (19,  6, 'v1.0.0-x64'),
    (20,  7, 'v1.0.0-x64'),
    (21,  9, 'v1.0.0-x64'),
    (22, 10, 'v1.0.0-x64'),
    (23,  1, 'v0.2.0-win-x86'),
    (24,  2, 'v0.2.0-win-amd64'),
    (25,  3, 'v0.2.0-linux-x86'),
    (26,  4, 'v0.2.0-linux-amd64'),
    (27,  1, 'v0.2.1-win-x86'),
    (28,  2, 'v0.2.1-win-amd64'),
    (29,  3, 'v0.2.1-linux-x86'),
    (30,  4, 'v0.2.1-linux-amd64'),
    (31,  1, 'v0.3.0-win-x86'),
    (32,  2, 'v0.3.0-win-amd64'),
    (33,  3, 'v0.3.0-linux-x86'),
    (34,  4, 'v0.3.0-linux-amd64'),
    (35,  1, 'v1.0.0-win-x86'),
    (36,  2, 'v1.0.0-win-amd64'),
    (37,  3, 'v1.0.0-linux-x86'),
    (38,  4, 'v1.0.0-linux-amd64'),
    (39,  1, 'v1.0.1-win-x86'),
    (40,  2, 'v1.0.1-win-amd64'),
    (41,  3, 'v1.0.1-linux-x86'),
    (42,  4, 'v1.0.1-linux-amd64'),
    (43,  1, 'v1.1.0-win-x86'),
    (44,  2, 'v1.1.0-win-amd64'),
    (45,  3, 'v1.1.0-linux-x86'),
    (46,  4, 'v1.1.0-linux-amd64'),
    (47,  5, 'Premium'),
    (48,  8, 'Premium');