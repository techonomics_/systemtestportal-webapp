// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build !ram

package store

import (
	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type stepRow struct {
	ID                int64 `xorm:"pk autoincr"`
	TestCaseVersionID int64
	StepIndex         int
	Action            string
	ExpectedResult    string
}

func stepFromRow(r stepRow) test.Step {
	return test.Step{
		Index:          r.StepIndex,
		Action:         r.Action,
		ExpectedResult: r.ExpectedResult,
	}
}

func rowFromStep(v test.Step) stepRow {
	return stepRow{
		StepIndex:      v.Index,
		Action:         v.Action,
		ExpectedResult: v.ExpectedResult,
	}
}

func saveSteps(s xorm.Interface, vrID int64, steps []test.Step) error {
	var srs []stepRow
	for _, step := range steps {
		sr := rowFromStep(step)
		sr.TestCaseVersionID = vrID
		srs = append(srs, sr)
	}

	// Since test versions can not be updated, test steps also can not be updated
	// Therefore we can assume that saving a test step means inserting it
	return insertStepRows(s, srs...)
}

func insertStepRows(s xorm.Interface, sr ...stepRow) error {
	_, err := s.Table(stepTable).Insert(&sr)
	return err
}

func listSteps(s xorm.Interface, vrID int64) ([]test.Step, error) {
	rows, err := listStepRows(s, vrID)
	if err != nil {
		return nil, err
	}

	var steps []test.Step
	for _, r := range rows {
		st := stepFromRow(r)
		steps = append(steps, st)
	}

	return steps, err
}

func listStepRows(s xorm.Interface, vrID int64) ([]stepRow, error) {
	var rows []stepRow

	err := s.Table(stepTable).Asc(stepIndexField).Find(&rows, &stepRow{TestCaseVersionID: vrID})
	if err != nil {
		return nil, err
	}

	return rows, nil
}
