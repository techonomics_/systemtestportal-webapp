/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package store

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/usersession"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

// Users defines the interfaces a store for users needs to fulfill
type Users interface {
	handler.UserLister
	creation.RegisterServer
	usersession.Auth
}

// Groups defines the interfaces a store for groups needs to fulfill
type Groups interface {
	handler.GroupLister
	middleware.GroupRetriever
	handler.GroupAdder
}

// Projects defines the interfaces a store for projects needs to fulfill
type Projects interface {
	List(string) ([]*project.Project, error)
	ListAll() ([]*project.Project, error)
	middleware.ProjectStore
	id.ProjectExistenceChecker
	handler.ProjectAdder
	handler.ProjectDeleter
}

// Cases defines the interfaces a store for test cases needs to fulfill
type Cases interface {
	handler.TestCaseLister
	middleware.TestCaseStore
	id.TestExistenceChecker
	handler.TestCaseAdder
	handler.TestCaseRenamer
	handler.TestCaseDeleter
}

// Sequences defines the interfaces a store for test sequences needs to fulfill
type Sequences interface {
	handler.TestSequenceLister
	middleware.TestSequenceStore
	id.TestExistenceChecker
	handler.TestSequenceAdder
	handler.TestSequenceRenamer
	handler.TestSequenceDeleter
}
