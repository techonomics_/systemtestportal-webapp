// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build !ram

package store

import (
	"fmt"

	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

const (
	variantField = "variant_id"
)

type sutVersionRow struct {
	ID        int64
	VariantID int64
	Name      string
}

func sutVersionFromRow(vr sutVersionRow) project.Version {
	return project.Version{
		Name: vr.Name,
	}
}

func rowFromSUTVersion(v project.Version) sutVersionRow {
	return sutVersionRow{
		Name: v.Name,
	}
}

func saveSUTVersions(s xorm.Interface, vrID int64, nvs []project.Version) error {
	ovrs, err := listSUTVersionRows(s, vrID)
	if err != nil {
		return err
	}

	var nvrs []sutVersionRow
	for _, nv := range nvs {
		nvr := rowFromSUTVersion(nv)
		nvr.VariantID = vrID
		nvrs = append(nvrs, nvr)
	}

	dvrs := versionRowSetMinus(ovrs, nvrs)
	uvrs := versionRowCutset(ovrs, nvrs)
	ivrs := versionRowSetMinus(nvrs, ovrs)

	err = deleteSUTVersionRows(s, dvrs...)
	if err != nil {
		return err
	}

	err = updateSUTVersionRows(s, uvrs...)
	if err != nil {
		return err
	}

	err = insertSUTVersionRows(s, ivrs...)
	return err
}

func insertSUTVersionRows(s xorm.Interface, vrs ...sutVersionRow) error {
	_, err := s.Table(sutVersionTable).Insert(&vrs)
	return err
}

func updateSUTVersionRows(s xorm.Interface, vrs ...sutVersionRow) error {
	for _, vr := range vrs {
		err := updateSUTVersionRow(s, &vr)
		if err != nil {
			return err
		}
	}

	return nil
}

func updateSUTVersionRow(s xorm.Interface, vr *sutVersionRow) error {
	aff, err := s.Table(sutVersionTable).ID(vr.ID).Update(vr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func deleteSUTVersionRows(s xorm.Interface, vrs ...sutVersionRow) error {
	var ids []int64
	for _, vr := range vrs {
		ids = append(ids, vr.ID)
	}

	_, err := s.Table(sutVersionTable).In(idField, ids).Delete(&sutVersionRow{})
	return err
}

func listSUTVersions(s xorm.Interface, vrID int64) ([]project.Version, error) {
	vrs, err := listSUTVersionRows(s, vrID)
	if err != nil {
		return nil, err
	}

	var vs []project.Version
	for _, vr := range vrs {
		v := sutVersionFromRow(vr)
		vs = append(vs, v)
	}

	return vs, nil
}

func listSUTVersionRows(s xorm.Interface, vrID int64) ([]sutVersionRow, error) {
	var vrs []sutVersionRow
	err := s.Table(sutVersionTable).Asc(idField).Find(&vrs, &sutVersionRow{VariantID: vrID})
	if err != nil {
		return nil, err
	}

	return vrs, nil
}

func lookupSUTVersionRowIDs(s xorm.Interface, vrID int64, names ...string) ([]int64, error) {
	var ids []int64
	err := s.Table(sutVersionTable).Distinct(idField).In(variantField, vrID).In(nameField, names).Find(&ids)
	if err != nil {
		return nil, err
	}

	return ids, nil
}

func versionRowSetMinus(s1, s2 []sutVersionRow) []sutVersionRow {
	rs := make([]sutVersionRow, len(s1))
	copy(rs, s1)

	for _, rvr := range s2 {
		for i := range rs {
			if rvr.Name == rs[i].Name {
				rs = append(rs[:i], rs[i+1:]...)
				break
			}
		}
	}

	return rs
}

func versionRowCutset(s1, s2 []sutVersionRow) []sutVersionRow {
	var rs []sutVersionRow
	for _, vr1 := range s1 {
		for _, vr2 := range s2 {
			if vr1.Name == vr2.Name {
				rs = append(rs, vr1)
				break
			}
		}
	}

	return rs
}
