// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build ram

package store

import (
	"sort"
	"strings"
	"sync"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

type projectsRAM struct {
	sync.RWMutex
	projects map[string]map[string]*project.Project
}

func initProjectsRAM() *projectsRAM {
	return &projectsRAM{
		projects: make(map[string]map[string]*project.Project),
	}
}

func (s *projectsRAM) List(cID string) ([]*project.Project, error) {
	if s == nil {
		panic("trying to list projects in nil store")
	}

	s.RLock()
	defer s.RUnlock()

	m, ok := s.projects[cID]
	if !ok {
		return nil, nil
	}

	var l []*project.Project
	for _, p := range m {
		l = append(l, p)
	}

	// sort the projects
	sort.Slice(l, func(i, j int) bool {
		cmp := strings.Compare(l[i].Name, l[j].Name)
		switch cmp {
		case 0:
			return false
		case 1:
			return false
		case -1:
			return true
		default:
			return false
		}
	})
	sortProjects(l)

	return l, nil
}

func (s *projectsRAM) ListAll() ([]*project.Project, error) {
	if s == nil {
		panic("trying to list projects in nil store")
	}

	s.RLock()
	defer s.RUnlock()

	var l []*project.Project
	for _, m := range s.projects {
		for _, p := range m {
			l = append(l, p)
		}
	}
	sortProjects(l)

	return l, nil
}

func (s *projectsRAM) Add(p *project.Project) error {
	if s == nil {
		panic("trying to add a project to a nil store")
	}

	if p == nil {
		panic("trying to add nil project to the store")
	}

	s.Lock()
	defer s.Unlock()

	m, ok := s.projects[p.Owner.Actor()]
	if !ok {
		m = make(map[string]*project.Project)
		s.projects[p.Owner.Actor()] = m
	}

	m[p.Name] = p

	return nil
}

func (s *projectsRAM) Get(pID id.ProjectID) (*project.Project, bool, error) {
	if s == nil {
		panic("trying to get project from nil store")
	}

	s.RLock()
	defer s.RUnlock()

	m, ok := s.projects[pID.Owner()]
	if !ok {
		return nil, false, nil
	}

	p, ok := m[pID.Project()]
	return p, ok, nil
}

func (s *projectsRAM) Delete(pID id.ProjectID) error {
	if s == nil {
		panic("trying to delete with nil store")
	}
	s.Lock()
	defer s.Unlock()

	m, ok := s.projects[pID.Owner()]
	if !ok {
		return nil
	}

	delete(m, pID.Project())
	return nil
}

func (s *projectsRAM) Exists(pID id.ProjectID) (bool, error) {
	if s == nil {
		panic("trying to get project from nil store")
	}

	s.RLock()
	defer s.RUnlock()

	m, ok := s.projects[pID.Owner()]
	if !ok {
		return false, nil
	}

	_, ok = m[pID.Project()]
	return ok, nil
}

func sortProjects(ps []*project.Project) {
	// sort the projects
	sort.Slice(ps, func(i, j int) bool {
		cmp := strings.Compare(ps[i].Name, ps[j].Name)
		switch cmp {
		case 0:
			return false
		case 1:
			return false
		case -1:
			return true
		default:
			return false
		}
	})
}
