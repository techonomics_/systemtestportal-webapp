/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package protocol

import (
	"errors"
	"log"
	"sort"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/store/dummydata"
)

// caseProtocolMap stores protocols for testcases
type caseProtocolMap map[string]map[string]map[string]map[int][]test.CaseExecutionProtocol

// sequenceProtocolMap stores protocols for testsequeneces
type sequenceProtocolMap map[string]map[string]map[string]map[int][]test.SequenceExecutionProtocol

//storeRAM provides functions for saving and loading execution-protocols
type storeRAM struct {
	// caseExecutionProtocols is a map of all CaseExecutionProtocols in the system.
	// It maps the testcaseName to the protocols of this TestCase.
	caseExecutionProtocols caseProtocolMap
	// sequenceExecutionProtocols is a map of all SequenceExecutionProtocols in the system.
	// It maps the name of a Sequence to its protocols.
	sequenceExecutionProtocols sequenceProtocolMap
}

// StoreRAM interface for storing test protocols.sstr
type StoreRAM interface {
	// AddCaseProtocol adds the given protocol to the store
	AddCaseProtocol(r *test.CaseExecutionProtocol) (err error)
	// AddSequenceProtocol adds the given protocol to the store
	AddSequenceProtocol(r *test.SequenceExecutionProtocol) (err error)
	// GetCaseExecutionProtocols gets the protocols for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocols(testcaseID id.TestID) ([]test.CaseExecutionProtocol, error)
	// GetCaseExecutionProtocols gets the protocols for all testcases of the project with given id.
	GetCaseExecutionProtocolsForProject(project id.ProjectID) ([]test.CaseExecutionProtocol, error)
	// GetCaseExecutionProtocol gets the protocol with the given id for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocol(protocolID id.ProtocolID) (test.CaseExecutionProtocol, error)
	// GetSequenceExecutionProtocols gets the protocols for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocols(sequenceID id.TestID) ([]test.SequenceExecutionProtocol, error)
	// GetSequenceExecutionProtocol gets the protocol with the given id for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocol(protocolID id.ProtocolID) (test.SequenceExecutionProtocol, error)
	// GetTestVersionProtocols gets the protocols for the testVersion with given id
	GetTestVersionProtocols(testVersionID id.TestVersionID) ([]id.ProtocolID, error)

	// HandleCaseRename updates the protocol store after a test case has been renamed
	HandleCaseRename(old, new id.TestID) error
	// HandleSequenceRename updates the protocol store after a test sequence has been renamed
	HandleSequenceRename(old, new id.TestID) error
}

var singleton storeRAM

func init() {
	singleton = storeRAM{
		caseExecutionProtocols:     make(caseProtocolMap),
		sequenceExecutionProtocols: make(sequenceProtocolMap),
	}

	initDummyCaseProtocols()
	initDummySequenceProtocols()
}

//GetProtocolStore returns the current store
func GetProtocolStore() StoreRAM {
	return &singleton
}

//AddCaseProtocol adds an CaseExecutionProtocol to the store and saves it
func (s *storeRAM) AddCaseProtocol(caseExecProtocol *test.CaseExecutionProtocol) (err error) {
	if caseExecProtocol == nil {
		return nil
	}

	pID := caseExecProtocol.ID()
	pOwner := pID.Owner()
	pProject := pID.Project()
	pTest := pID.Test()
	pVersion := pID.TestVersion()

	om := s.caseExecutionProtocols[pOwner]
	if om == nil {
		s.caseExecutionProtocols[pOwner] =
			make(map[string]map[string]map[int][]test.CaseExecutionProtocol)
	}
	pm := om[pProject]
	if pm == nil {
		s.caseExecutionProtocols[pOwner][pProject] = make(map[string]map[int][]test.CaseExecutionProtocol)
	}
	vm := pm[pTest]
	if vm == nil {
		s.caseExecutionProtocols[pOwner][pProject][pTest] = make(map[int][]test.CaseExecutionProtocol)
	}
	protocols := vm[pVersion]
	s.caseExecutionProtocols[pOwner][pProject][pTest][pVersion] = append(protocols, *caseExecProtocol)

	return nil
}

//AddSequenceProtocol adds an SequenceExecutionProtocol to the store and saves it
func (s *storeRAM) AddSequenceProtocol(r *test.SequenceExecutionProtocol) (err error) {
	if r == nil {
		return nil
	}

	om := s.sequenceExecutionProtocols[r.ID().Owner()]
	if om == nil {
		s.sequenceExecutionProtocols[r.ID().Owner()] =
			make(map[string]map[string]map[int][]test.SequenceExecutionProtocol)
	}
	pm := om[r.ID().Project()]
	if pm == nil {
		s.sequenceExecutionProtocols[r.ID().Owner()][r.ID().Project()] =
			make(map[string]map[int][]test.SequenceExecutionProtocol)
	}
	vm := pm[r.ID().Test()]
	if vm == nil {
		s.sequenceExecutionProtocols[r.ID().Owner()][r.ID().Project()][r.ID().Test()] =
			make(map[int][]test.SequenceExecutionProtocol)
	}
	protocols := vm[r.ID().TestVersion()]
	s.sequenceExecutionProtocols[r.ID().Owner()][r.ID().Project()][r.ID().Test()][r.ID().TestVersion()] =
		append(protocols, *r)
	return nil
}

//GetCaseExecutionProtocols returns a slice of all CaseExecutionProtocols, that belongs to the given test case.
func (s *storeRAM) GetCaseExecutionProtocols(testcaseID id.TestID) ([]test.CaseExecutionProtocol, error) {
	vm := s.caseExecutionProtocols[testcaseID.Owner()][testcaseID.Project()][testcaseID.Test()]
	var result []test.CaseExecutionProtocol
	for _, v := range vm {
		result = append(result, v...)
	}

	sort.Sort(sort.Reverse(byExecutionDate(result)))

	return result, nil
}

func (s *storeRAM) GetCaseExecutionProtocolsForProject(pID id.ProjectID) ([]test.CaseExecutionProtocol, error) {
	cm := s.caseExecutionProtocols[pID.Owner()][pID.Project()]
	var result []test.CaseExecutionProtocol
	for tc := range cm {
		ceps, err := s.GetCaseExecutionProtocols(id.NewTestID(pID, tc, true))
		if err != nil {
			return nil, err
		}

		result = append(result, ceps...)
	}

	sort.Sort(sort.Reverse(byExecutionDate(result)))

	return result, nil
}

// GetSequenceExecutionProtocols returns a slice of all SequenceExecutionProtocol,
// that belongs to the given test sequence.
func (s *storeRAM) GetSequenceExecutionProtocols(sequenceID id.TestID) ([]test.SequenceExecutionProtocol, error) {
	vm := s.sequenceExecutionProtocols[sequenceID.Owner()][sequenceID.Project()][sequenceID.Test()]
	var result []test.SequenceExecutionProtocol
	for _, v := range vm {
		result = append(result, v...)
	}
	return result, nil
}

//GetCaseExecutionProtocols returns a slice of all CaseExecutionProtocols, that belongs to the given test case.
func (s *storeRAM) GetCaseExecutionProtocol(protocolID id.ProtocolID) (test.CaseExecutionProtocol, error) {

	slice, err := s.GetCaseExecutionProtocols(protocolID.TestID)
	if err != nil {
		return test.CaseExecutionProtocol{}, err
	}
	for _, prt := range slice {
		if prt.ID() == protocolID {
			return prt, nil
		}
	}

	return test.CaseExecutionProtocol{}, nil
}

//GetSequenceExecutionProtocol returns the SequenceExecutionProtocol, that fits to the given id data.
func (s *storeRAM) GetSequenceExecutionProtocol(protocolID id.ProtocolID) (test.SequenceExecutionProtocol, error) {
	slice, err := s.GetSequenceExecutionProtocols(protocolID.TestID)
	if err != nil {
		return test.SequenceExecutionProtocol{}, err
	}
	for _, prt := range slice {
		if prt.ID() == protocolID {
			return prt, nil
		}
	}

	return test.SequenceExecutionProtocol{}, nil
}

func (s *storeRAM) GetTestVersionProtocols(tvID id.TestVersionID) (result []id.ProtocolID, err error) {
	if tvID.IsCase() {
		protocols := s.caseExecutionProtocols[tvID.Owner()][tvID.Project()][tvID.Test()][tvID.TestVersion()]

		for _, v := range protocols {
			result = append(result, v.ID())
		}
	} else {
		protocols := s.sequenceExecutionProtocols[tvID.Owner()][tvID.Project()][tvID.Test()][tvID.TestVersion()]

		for _, v := range protocols {
			result = append(result, v.ID())
		}
	}

	return
}

func (s *storeRAM) HandleCaseRename(old, new id.TestID) error {
	if old.ProjectID != new.ProjectID {
		return errors.New(`unsupported operation: test cases currently can not be moved between projects`)
	}

	om := s.caseExecutionProtocols[old.Owner()]
	if om == nil {
		return nil
	}

	pm := om[old.Project()]
	if pm == nil {
		return nil
	}

	ops := pm[old.Test()]
	if ops == nil {
		return nil
	}

	nps := make(map[int][]test.CaseExecutionProtocol)

	for k, ovps := range ops {
		var nvps []test.CaseExecutionProtocol

		for _, op := range ovps {
			np := op.DeepCopy()
			np.TestVersion.TestID = new
			nvps = append(nvps, np)
		}

		nps[k] = nvps
	}

	delete(pm, old.Test())
	pm[new.Test()] = nps

	return nil
}

func (s *storeRAM) HandleSequenceRename(old, new id.TestID) error {
	if old.ProjectID != new.ProjectID {
		return errors.New(`unsupported operation: test sequences currently can not be moved between projects`)
	}

	om := s.sequenceExecutionProtocols[old.Owner()]
	if om == nil {
		return nil
	}

	pm := om[old.Project()]
	if pm == nil {
		return nil
	}

	ops := pm[old.Test()]
	if ops == nil {
		return nil
	}

	nps := make(map[int][]test.SequenceExecutionProtocol)

	for k, ovps := range ops {
		var nvps []test.SequenceExecutionProtocol

		for _, op := range ovps {
			np := op
			np.TestVersion.TestID = new
			nvps = append(nvps, np)
		}

		nps[k] = nvps
	}

	delete(pm, old.Test())
	pm[new.Test()] = nps

	return nil
}

func initDummyCaseProtocols() {
	for _, dcp := range dummydata.CaseProtocols {
		cp := new(test.CaseExecutionProtocol)
		*cp = dcp
		err := singleton.AddCaseProtocol(cp)
		if err != nil {
			log.Println(err)
		}
	}
}

func initDummySequenceProtocols() {
	for _, dsp := range dummydata.SequenceProtocols() {
		sp := new(test.SequenceExecutionProtocol)
		*sp = dsp

		err := singleton.AddSequenceProtocol(sp)
		if err != nil {
			log.Println(err)
		}
	}
}
