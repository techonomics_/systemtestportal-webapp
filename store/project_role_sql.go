// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build !ram

package store

import (
	"fmt"

	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

const errorNoRole = "no role with the name %v"

type projectRoleRow struct {
	ID        int64
	ProjectID int64
	Name      string

	Execute bool

	CreateCase    bool
	EditCase      bool
	DeleteCase    bool
	DuplicateCase bool
	AssignCase    bool

	CreateSequence    bool
	EditSequence      bool
	DeleteSequence    bool
	DuplicateSequence bool
	AssignSequence    bool

	EditMembers bool

	EditProject     bool // Change name, description, image, visibility
	DeleteProject   bool
	EditPermissions bool
}

func getRoleRowByName(s xorm.Interface, name string) (projectRoleRow, bool, error) {
	rr := projectRoleRow{Name: name}
	ex, err := s.Table(projectRolesTable).Get(&rr)
	if err != nil || !ex {
		return projectRoleRow{}, false, err
	}
	return rr, true, nil
}

func saveProjectRoles(s xorm.Interface, prID int64, nprs map[project.RoleName]*project.Role) error {
	oprrs, err := listProjectRoleRowsForProject(s, prID)
	if err != nil {
		return err
	}

	var nprrs []projectRoleRow
	for _, npr := range nprs {
		nprr := rowFromProjectRole(npr)
		nprr.ProjectID = prID
		nprrs = append(nprrs, nprr)
	}

	// Delete roles that are no longer in the project
	dprrs := projectRoleRowsSetMinus(oprrs, nprrs)
	err = deleteProjectRoleRows(s, dprrs...)
	if err != nil {
		return err
	}

	// Update existing roles that might have changed
	uprrs := projectRoleRowCutSet(oprrs, nprrs)
	err = updateProjectRoleRows(s, uprrs...)
	if err != nil {
		return err
	}

	// Insert new roles
	iprrs := projectRoleRowsSetMinus(nprrs, oprrs)
	err = insertProjectRoleRows(s, iprrs...)

	return err
}

func listProjectRoles(s xorm.Interface, prID int64) (map[project.RoleName]*project.Role, error) {
	rrs, err := listProjectRoleRowsForProject(s, prID)
	if err != nil {
		return nil, err
	}

	rs := make(map[project.RoleName]*project.Role)
	for _, rr := range rrs {
		r := projectRoleFromRow(rr)
		rs[r.Name] = r
	}
	return rs, nil
}

func listProjectRoleRowsForProject(s xorm.Interface, prID int64) ([]projectRoleRow, error) {
	var prrs []projectRoleRow
	err := s.Table(projectRolesTable).Asc(nameField).Find(&prrs, &projectRoleRow{ProjectID: prID})
	if err != nil {
		return nil, err
	}
	return prrs, nil
}

func insertProjectRoleRows(s xorm.Interface, prr ...projectRoleRow) error {
	_, err := s.Table(projectRolesTable).Insert(&prr)
	return err
}

func projectRoleRowsSetMinus(s1, s2 []projectRoleRow) []projectRoleRow {
	var rs []projectRoleRow
	for _, prr1 := range s1 {
		if !projectRoleRowSetContains(s2, prr1) {
			rs = append(rs, prr1)
		}
	}
	return rs
}

func deleteProjectRoleRows(s xorm.Interface, prrs ...projectRoleRow) error {
	var ids []int64
	for _, prr := range prrs {
		ids = append(ids, prr.ID)
	}

	_, err := s.Table(projectRolesTable).In(idField, ids).Delete(&projectRoleRow{})
	return err
}

func projectRoleRowSetContains(prrs []projectRoleRow, e projectRoleRow) bool {
	for _, prr := range prrs {
		if prr.Name == e.Name {
			return true
		}
	}
	return false
}

func projectRoleRowCutSet(s1, s2 []projectRoleRow) []projectRoleRow {
	var rs []projectRoleRow
	for _, prr1 := range s1 {
		for _, prr2 := range s2 {
			if prr1.Name == prr2.Name {
				rprr := prr2
				rprr.ID = prr1.ID
				rs = append(rs, rprr)
			}
		}
	}
	return rs
}

func updateProjectRoleRows(s xorm.Interface, prrs ...projectRoleRow) error {
	for _, prr := range prrs {
		err := updateProjectRoleRow(s, &prr)
		if err != nil {
			return err
		}
	}
	return nil
}

func updateProjectRoleRow(s xorm.Interface, prr *projectRoleRow) error {
	aff, err := s.Table(projectRolesTable).ID(prr.ID).Update(prr)
	if err != nil {
		return err
	}
	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}
	return nil
}

func getProjectRoleByID(s xorm.Interface, rID int64) (*project.Role, bool, error) {
	rr, ex, err := getProjectRoleRowByID(s, rID)
	if err != nil || !ex {
		return nil, false, err
	}

	return projectRoleFromRow(rr), true, nil
}

func getProjectRoleRowByID(s xorm.Interface, rID int64) (projectRoleRow, bool, error) {
	rr := projectRoleRow{ID: rID}
	ex, err := s.Table(projectRolesTable).Get(&rr)
	if err != nil || !ex {
		return projectRoleRow{}, false, err
	}
	return rr, true, nil
}

func projectRoleFromRow(rr projectRoleRow) *project.Role {
	return &project.Role{
		Name: project.RoleName(rr.Name),
		Permissions: project.Permissions{
			ExecutionPermissions: project.ExecutionPermissions{
				Execute: rr.Execute,
			},
			CasePermissions: project.CasePermissions{
				CreateCase:    rr.CreateCase,
				EditCase:      rr.EditCase,
				DeleteCase:    rr.DeleteCase,
				AssignCase:    rr.AssignCase,
				DuplicateCase: rr.DuplicateCase,
			},
			SequencePermissions: project.SequencePermissions{
				CreateSequence:    rr.CreateSequence,
				EditSequence:      rr.EditSequence,
				DeleteSequence:    rr.DeleteSequence,
				AssignSequence:    rr.AssignSequence,
				DuplicateSequence: rr.DuplicateSequence,
			},
			MemberPermissions: project.MemberPermissions{
				EditMembers: rr.EditMembers,
			},
			SettingsPermissions: project.SettingsPermissions{
				EditProject:     rr.EditProject,
				DeleteProject:   rr.DeleteProject,
				EditPermissions: rr.EditPermissions,
			},
		},
	}
}

func rowFromProjectRole(pr *project.Role) projectRoleRow {
	return projectRoleRow{
		Name:    pr.Name.String(),
		Execute: pr.Permissions.Execute,

		CreateCase:    pr.Permissions.CreateCase,
		EditCase:      pr.Permissions.EditCase,
		DeleteCase:    pr.Permissions.DeleteCase,
		DuplicateCase: pr.Permissions.DuplicateCase,
		AssignCase:    pr.Permissions.AssignCase,

		CreateSequence:    pr.Permissions.CreateSequence,
		EditSequence:      pr.Permissions.EditSequence,
		DeleteSequence:    pr.Permissions.DeleteSequence,
		DuplicateSequence: pr.Permissions.DuplicateSequence,
		AssignSequence:    pr.Permissions.AssignSequence,

		EditMembers: pr.Permissions.EditMembers,

		EditProject:     pr.Permissions.EditProject,
		DeleteProject:   pr.Permissions.DeleteProject,
		EditPermissions: pr.Permissions.EditPermissions,
	}
}
