# Workflow

In the following guide placeholders in code blocks will be enclosed by angle brackets `<>`. These should be replaced with the respective value.

## Requirements
- Git is required for contributing this project. Follow [these instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) to install and [set up](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup) Git on your system, in case you haven't already. 
  
- This project uses Golang. Follow [these instructions](https://golang.org/doc/install) to install Golang on your system. If you already have a previous installation of Golang, or are installing from another source, make sure that the version is 1.8 or newer. Ensure that $GOPATH is set:  
    ```bash
    go version
    ```

## Set up the project
 
- Clone the repository into `$GOPATH/src/gitlab.com/stp-team/systemtestportal-webapp/`  

    ```bash
    cd $GOPATH
    mkdir -p src/gitlab.com/stp-team/
    cd src/gitlab.com/stp-team
    git clone git@gitlab.com:stp-team/systemtestportal-webapp systemtestportal-webapp
    cd /systemtestportal-webapp
    ```
## Build the project
1. Go to the /systemtestportal-webapp directory

2. There currently exist two possibilities when building the project: 
- With volatile storage, all changes will be lost when stopping the program
    ```bash
    go build --tags 'ram' ./cmd/stp
    ```
- With a persistent sqlite data base
    ```bash
    go ./cmd/stp
    ```

3. To run the project on localhost:8080 run
    ```bash
    ./stp --basepath=<repository directory> --data-dir=<directory containing .db file>
    ```  
    
4. See the [configuration section](https://gitlab.com/stp-team/systemtestportal-webapp#configuration) for further flags and information about the config file
  
## Contribute to the project

1.  Choose an issue and assign yourself. If there are people already assigned to the issue, please talk to them before adding yourself to the assignees. In this case you might also want to skip the next step.

    ![issue_page_assign](/uploads/f883ce2c1313bc517d01e7bf44840479/issue_page_assign.png)

1.  Next create a branch and merge request for the issue
    
    ![issue_page_merge_request](/uploads/0f57fc088868367867e6299a1138a53d/issue_page_merge_request.png)

1. Set the issue label do `Status/Doing`  

1.  Update your local project and switch to the new branch. Again in git bash this would be done by

    ```bash
    # Assuming you are in the project directory
    git pull --ff-only
    git checkout <branch>
    ```

1.  Make your changes and use `git add` and `git commit` to track them

1.  Integrate your local changes with the upstream changes and publish the result

    ```bash
    git pull --rebase
    git push
    ```

1.  Be sure your branch meets the Defintion of Done.

2.  When you think the branch is ready for review, remove the `WIP` tag from the merge request title, this can be done with the slash command `/wip`. Also, update the label from `Status/Doing` to `Status/Review`. 

1.  Add label `Status/Review` and a QA as assignee to your merge request. You can check who can approve the merge request in the requires approvel line. 

2.  Once the merge request has been approved by at least one QA person it can be merged into `master`.  

1.  After that you can remove your local feature branch with:
    
    ```bash
    git branch -d <branch>
    ```
    

### Approving

1. As QA you can start a review runner, so you do not have to pull the branch and build it yourself.

2. Be sure to stop the review after you are done.

3. Fill the Definition of Done checklist.

4. Review code for good quality.

5. If you find something wrong with the code or something conflicting with the Definition of Done you can start a discussion, which needs to be resolved before merging.

    
## Guidelines

### Naming conventions

Please have a look at the [naming conventions](https://gitlab.com/stp-team/systemtestportal-webapp/wikis/contribute/Naming-conventions) in the wiki.